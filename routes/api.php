<?php

use App\Http\Controllers\FoodsApiController;
use App\Http\Controllers\ShoppingListApiController;
use App\Http\Controllers\IngredientsApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/foods/decide/', [FoodsApiController::class, 'decide'])->name('api.food.decide');
Route::get('/foods/get-all/', [FoodsApiController::class, 'getAll'])->name('api.food.getAll');
Route::get('/foods/{id}/get/', [FoodsApiController::class, 'getFoodById'])->name('api.food.get');
Route::get('/foods/{id}/accept/', [FoodsApiController::class, 'accept'])->name('api.food.accept');
Route::post('/foods/create/', [FoodsApiController::class, 'create'])->name('api.food.create');
Route::post('/foods/{id}/new-ingredient/', [FoodsApiController::class, 'newIngredient'])->name('api.food.new-ingredient');
Route::post('/foods/{id}/update-recipe/', [FoodsApiController::class, 'updateRecipe'])->name('api.food.update-recipe');
Route::post('/foods/{id}/clear-cooldown/', [FoodsApiController::class, 'clearCooldown'])->name('api.food.clear-cooldown');
Route::delete('/foods/{id}/remove-ingredient/{ingredientId}', [FoodsApiController::class, 'removeIngredient'])->name('api.food.remove-ingredient');
Route::delete('/foods/{id}/delete-food', [FoodsApiController::class, 'deleteFood'])->name('api.food.delete-food');

Route::get('/ingredients/get-all/', [IngredientsApiController::class, 'getAll'])->name('api.ingredient.getAll');

Route::post('/shopping/add/', [ShoppingListApiController::class, 'add'])->name('api.shopping-list.add');
