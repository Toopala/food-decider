<?php

namespace Database\Factories;

use App\Models\Food;
use Illuminate\Database\Eloquent\Factories\Factory;

class FoodFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->word(),
            'cooldown' => $this->faker->numberBetween(1,12),
            'cooldownPeriod' => $this->faker->numberBetween(1,3),
            'note' => $this->faker->paragraph(),
        ];
    }
}
