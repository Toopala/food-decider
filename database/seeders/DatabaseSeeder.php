<?php

namespace Database\Seeders;

use App\Models\Food;
use App\Models\Ingredient;
use Database\Factories\IngredientFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $ingredientsToCreate = 20;
        $foodsToCreate = 10;
        $ingredients = Ingredient::factory($ingredientsToCreate)->create();

        for ($i = 0; $i < $foodsToCreate; $i++) {
            Food::factory()->count(1)->hasAttached($ingredients->random(rand(3, 7)))->create();
        }
    }
}
