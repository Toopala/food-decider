module.exports = {
    mode: "jit",
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.ts",
        "./resources/**/*.tsx"
    ],
    theme: {
        container: {
            center: true
        },
        extend: {},
    },
    plugins: [
    ],
}
