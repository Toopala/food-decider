<?php

namespace App\Http\Controllers;

use App\Models\Ingredient;
use Illuminate\Http\Response;

class IngredientsApiController extends Controller
{
    public function getAll(): Response
    {
        return new Response(Ingredient::all()->toArray(), 200);
    }
}
