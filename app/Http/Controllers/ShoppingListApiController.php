<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\Ingredient;
use Illuminate\Support\Facades\Log;

class ShoppingListApiController extends Controller
{
    public function add(Request $request): Response
    {
        $ingredient = Ingredient::find($request->id);

        $client = new Client(["base_uri" => "http://supervisor/", "verify" => false]);

        $response = $client->request("POST", "core/api/services/shopping_list/add_item",
            [
            "headers" => [
                "authorization" => "Bearer " . env("SUPERVISOR_TOKEN")
            ],
                "body" => json_encode(["name" => "$ingredient->title $request->itemAmount"]),
        ]);

        return new Response($response->getStatusCode());
    }
}
