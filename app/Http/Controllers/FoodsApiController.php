<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\Ingredient;
use App\Services\FoodService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;


class FoodsApiController extends Controller
{
    public function create(Request $request): Response
    {
        try {
            $request->validate([
                "title" => "required|string|min:3",
                "ingredients" => "required|string",
                "cooldown" => "required|int",
                "cooldownPeriod" => "int|digits_between:0,3",
                "notes" => "string|min:0|nullable"
            ]);
        } catch (ValidationException $e) {
            return new Response($e->errors(), 400);
        }

        try {
            DB::beginTransaction();
            $food = Food::updateOrCreate(
                [
                    "title" => ucfirst(trim($request->title)),
                ],
                [
                    "cooldown" => $request->cooldown,
                    "cooldownPeriod" => Food::COOLDOWN_PERIODS[$request->cooldownPeriod],
                    "note" => $request->notes,
                ]
            );
            foreach (explode(",", $request->ingredients) as $ingredient) {
                FoodService::addNewIngredient($food->id, $ingredient);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return new Response("Failed {$e->getMessage()}", 400);
        }

        return new Response(FoodService::getFoodWithIngredients($food->id));
    }

    public function decide(): Response
    {
        return new Response(FoodService::getRandomFoodOffCooldown(), 200);
    }

    public function accept(Request $request): Response
    {
        $acceptedFood = Food::find($request->id);

        $acceptedFood->accepted_at = Carbon::now();
        $acceptedFood->cooldown_clears_at = Carbon::now()->add($acceptedFood->cooldown, $acceptedFood->cooldownPeriod);
        $acceptedFood->save();

        return new Response($acceptedFood, 200);
    }

    public function updateRecipe(Request $request): Response
    {
        try {
            $food = Food::find($request->id);
            $food->note = $request->recipe;
            $food->save();
            return new Response($food->note, 200);
        } catch (\Exception $e) {
            return new Response(400);
        }
    }

    public function clearCooldown(Request $request): Response
    {
        try {
            $food = Food::find($request->id);
            $food->cooldown_clears_at = Carbon::now()->toDateString();
            $food->save();
            return new Response($food, 200);
        } catch (\Exception $e) {
            return new Response(400);
        }
    }

    public function newIngredient(Request $request): Response
    {
        try {
            FoodService::addNewIngredient($request->foodId, $request->ingredientName);
            return new Response(Ingredient::getByTitle($request->ingredientName)->toJson(), 200);
        } catch (\Exception $e) {
            return new Response(400);
        }
    }

    public function removeIngredient(Request $request): Response
    {
        try {
            Food::find($request->id)->ingredients()->detach($request->ingredientId);
            return new Response(200);
        } catch (\Exception $e) {
            return new Response(400);
        }
    }

    public function deleteFood(Request $request): Response
    {
        try {
            $food = Food::find($request->id);
            $food->ingredients()->detach();
            $food->delete();

            return new Response(200);
        } catch (\Exception $e) {
            return new Response(400);
        }
    }

    public function getFoodById(Request $request): Response
    {
        return new Response(FoodService::getFoodWithIngredients($request->id), 200);
    }

    public function getAll(): Response
    {
        return new Response(Food::all()->toArray(), 200);
    }
}
