<?php

namespace App\Services;

use App\Models\Food;
use App\Models\Ingredient;
use Carbon\Carbon;

class FoodService
{
    public static function getFoodWithIngredients($foodId)
    {
        $food = Food::where('id', '=', $foodId)->first();

        $ingredients = [];
        foreach ($food->ingredients as $ingredient) {
            $ingredients[] = $ingredient->toArray();
        }

        $result = $food->toArray();
        $result["ingredients"] = $ingredients;
        return $result;
    }

    public static function addNewIngredient($foodId, $ingredientName)
    {
        Food::find($foodId)->ingredients()->syncWithoutDetaching(Ingredient::updateOrCreate(
            [
                "title" => trim(strtolower($ingredientName))
            ])->id);
    }

    public static function getRandomFoodOffCooldown()
    {
        return Food::where('cooldown_clears_at', null)
            ->orWhere('cooldown_clears_at', '<', Carbon::now())->get()->random(1)->first();
    }
}
