<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    use HasFactory;
    protected $table = "foods";
    protected $fillable = ["title", "cooldown", "cooldownPeriod", "note"];
    public const COOLDOWN_PERIODS = ["day", "week", "month", "year"];

    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class);
    }
}
