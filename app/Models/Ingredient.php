<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    use HasFactory;
    protected $table = "ingredients";
    protected $fillable = ["title"];
    public $timestamps = false;


    public static function getByTitle($title)
    {
        return Ingredient::where('title', trim(strtolower($title)))->first();
    }

    public function foods()
    {
        return $this->belongsToMany(Food::class);
    }

}
