<?php

namespace Tests\Feature;

use App\Models\Food;
use App\Models\Ingredient;
use App\Models\FoodIngredient;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class FoodsControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testView()
    {
        $food = Food::factory()->create();
        $ingredient = Ingredient::factory()->count(3)->create();

        $ingredient->each(function ($ingredient) use ($food) {
            FoodIngredient::factory()->create(
                [
                    "food_id" => $food->id,
                    "ingredient_id" => $ingredient->id
                ]
            );
        });

        $response = $this->get("/foods/view/{$food->id}");


        $returnValue = $food->toArray();
        $returnValue["ingredients"] = $ingredient->toArray();


        $response->assertViewHas("food");
        $response->assertStatus(200);
    }
}
