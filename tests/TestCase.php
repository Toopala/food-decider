<?php

namespace Tests;

use Illuminate\Database\Console\Migrations\MigrateCommand;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
}
