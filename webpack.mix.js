
const mix = require('laravel-mix');
mix.disableNotifications();

mix.js("resources/js/app.ts", "public/js")
    .postCss("resources/css/app.css", "public/css",
        [
            require("tailwindcss"),
        ])
    .webpackConfig({
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: "ts-loader",
                    exclude: /node_modules/
                }
            ]
        },
        resolve: {
            extensions: ["*", ".js", ".jsx", ".vue", ".ts", ".tsx"]
        }
    })
    .react();
