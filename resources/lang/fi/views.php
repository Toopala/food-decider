<?php

return [
    'header' => [
        "home" => "Alkuun"
    ],
    'common' => [
        "food_decider" => "Ruuanpäätöskone",
        "create_new_food" => "Lisää uusi ruoka",
        "submit" => "Lähetä",
        "ingredients" => "Raaka-aineet",
        "add" => "Lisää",
        "add_all" => "Lisää kaikki",
        "recipe" => "Resepti",
        "edit" => "Muokkaa",
        "done" => "Valmis",
        "save" => "Tallenna",
        "delete" => "Poista",
    ],
    'create' => [
        "food_title" => "Ruoan nimi",
        "food_title_placeholder" => "Ruoan nimi",
        "ingredients_placeholder" => "Pilkkuerotettu lista: kala, peruna..",
        "cooldown" => "Karenssi",
        "cooldown_placeholder" => "Kuinka monta ajanyksikköä karenssi kestää",
        "unit_of_time" => "Aikayksikkö",
        "days" => "Päivä",
        "weeks" => "Viikko",
        "months" => "Kuukausi",
        "years" => "Vuosi",
        "recipe" => "Resepti ja vinkkejä",
        "recipe_placeholder" => "Kirjoita resepti ja/tai vinkkejä ruoan onnistumiseksi",
    ],
    "index" => [
        "welcome" => "Tervetuloa ruoanpäätöskoneeseen",
        "create_new_food" => "Lisää uusi ruoka",
        "show_all_foods" => "Näytä tallennetut ruuat",
        "decide" => "Tee päätös",
        "decision_made" => "Päätöksesi on :Food!",
    ],
    "view" => [
        "amount" => "Lukumäärä",
        "new_ingredient_title" => "Uusi raaka-aine",
        "add_new_ingredient" => "Lisää ruokaan uusi raaka-aine",
        "no_recipe" => "Ei reseptiä",
    ],
];
