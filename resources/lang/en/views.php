<?php

return [
    'header' => [
        "home" => "Home"
    ],
    'common' => [
        "food_decider" => "Food decider",
        "create_new_food" => "Add new food",
        "submit" => "Submit",
        "ingredients" => "Ingredients",
        "add" => "Add",
        "add_all" => "Add all",
        "recipe" => "Recipe",
        "edit" => "Edit",
        "done" => "Done",
        "save" => "Save",
        "delete" => "Delete",
    ],
    'create' => [
        "food_title" => "Food title",
        "food_title_placeholder" => "Food title",
        "ingredients_placeholder" => "Comma separated list: fish, potatoes...",
        "cooldown" => "Cooldown",
        "cooldown_placeholder" => "How many units of time cooldown lasts",
        "unit_of_time" => "Unit of time",
        "days" => "Days",
        "weeks" => "Weeks",
        "months" => "Months",
        "years" => "years",
        "recipe" => "Food recipe and notes",
        "recipe_placeholder" => "Write down recipe and tips and tricks to make delicious food",
    ],
    "index" => [
        "welcome" => "Welcome to food decider",
        "create_new_food" => "Create new food",
        "show_all_foods" => "Show all saved foods",
        "decide" => "Decide",
        "decision_made" => ":Food was decided for you!",
    ],
    "view" => [
        "amount" => "Item amount",
        "new_ingredient_title" => "New ingredient name",
        "add_new_ingredient" => "Add new ingredient to food",
        "no_recipe" => "No recipe",
    ],
];
