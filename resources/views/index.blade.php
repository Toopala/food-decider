<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="UTF-8">
    <title>{{ __("views.common.food_decider") }}</title>
    <script>
        Window.HASSIO_APP_URL = '{{ Config::get("app.hassio_url") }}';
        Window.LOCALE = '{{ Config::get("app.locale") }}';
    </script>
    <script src="{{ Config::get("app.hassio_url") }}/js/app.js" defer></script>
    <link href="{{ Config::get("app.hassio_url") }}/css/app.css" rel="stylesheet">
</head>
<body>
<div id="root"></div>
</body>
</html>
