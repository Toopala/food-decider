import i18next from 'i18next'
import { initReactI18next } from 'react-i18next'

//Import all translation files
import { lang as translationEnglish } from './lang/en'
import { lang as translationFinnish } from './lang/fi'

const resources = {
    en: {
        home: translationEnglish,
    },
    fi: {
        home: translationFinnish,
    },
}

i18next.use(initReactI18next).init({
    resources,
    //@ts-ignore
    lng: Window.LOCALE || 'fi', //default language
})

export default i18next
