import React from 'react'
import App from './components/App'
import Create from './components/Create'
import Foods from './components/Foods'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Navbar from './components/Navbar'
import ViewFood from './components/ViewFood'
import { createRoot } from 'react-dom/client'

const container = document.getElementById('root')

const root = createRoot(container!)

// @ts-ignore
export const urlPrefix = Window.HASSIO_APP_URL
// @ts-ignore

root.render(
    <div className="background min-h-screen py-2">
        <Router>
            <Routes>
                <Route path={`${urlPrefix}/`} element={<App />} />
                <Route path={`${urlPrefix}/foods`} element={<Navbar />}>
                    <Route path="create" element={<Create />} />
                    <Route path="all" element={<Foods />} />
                    <Route path="view/:id" element={<ViewFood />} />
                </Route>
            </Routes>
        </Router>
    </div>
)
