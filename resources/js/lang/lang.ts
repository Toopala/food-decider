export interface Lang {
    header: { home: string }
    common: {
        add: string
        create_new_food: string
        submit: string
        edit: string
        food_decider: string
        recipe: string
        save: string
        ingredients: string
        add_all: string
        done: string
        delete: string
        cancel: string
    }
    create: {
        food_title_placeholder: string
        unit_of_time: string
        weeks: string
        months: string
        recipe_placeholder: string
        food_title: string
        cooldown_placeholder: string
        cooldown: string
        recipe: string
        ingredients_placeholder: string
        days: string
        years: string
        food_create_failed: string
        food_created: string
    }
    index: {
        create_new_food: string
        decide: string
        show_all_foods: string
        welcome: string
        decision_made: string
    }
    view: {
        amount: string
        new_ingredient_placeholder: string
        add_new_ingredient: string
        add_all_to_shopping_list: string
        no_recipe: string
        no_foods: string
        no_ingredients: string
        clear_cooldown: string
    }
}
