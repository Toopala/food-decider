import { Link, Outlet, useNavigate } from 'react-router-dom'
import {
    MdHome,
    MdList,
    MdOutlineKeyboardBackspace,
    MdPlaylistAdd,
} from 'react-icons/md'
import React from 'react'
import { urlPrefix } from '../index'
import { SmallButton } from './Button'

const Navbar = () => {
    const navigate = useNavigate()

    return (
        <>
            <div className="flex flex-row justify-center gap-4">
                <span>
                    <SmallButton
                        icon={MdOutlineKeyboardBackspace}
                        onClick={() => {
                            navigate(-1)
                        }}
                    />
                </span>
                <Link to={`${urlPrefix}/`}>
                    <SmallButton icon={MdHome} />
                </Link>
                <Link to={`${urlPrefix}/foods/create`}>
                    <SmallButton icon={MdPlaylistAdd} />
                </Link>
                <Link to={`${urlPrefix}/foods/all`}>
                    <SmallButton icon={MdList} />
                </Link>
            </div>
            <Outlet />
        </>
    )
}

export default Navbar
