import React, { useEffect, useState } from 'react'
import { Button, Styles } from './Button'
import { Link } from 'react-router-dom'
import { urlPrefix } from '../index'
import { useTranslation } from 'react-i18next'

export interface Food {
    title: string
    ingredients: Ingredient[]
    cooldown: number
    cooldownPeriod: number
    cooldown_clears_at: string | null
    isOnCooldown?: boolean
    note: string
    id: number
    error: boolean
}

export interface Ingredient {
    title: string
    id: number
}

interface FoodItemProps {
    key: number
    food: Food
    onDelete: (id: number) => Promise<void>
}

const FoodItem: React.FC<FoodItemProps> = ({ food, onDelete }) => {
    const { t } = useTranslation(['home'])
    const isOnCooldown = () => {
        return new Date() < new Date(food.cooldown_clears_at)
    }
    const style = isOnCooldown() ? Styles.notice : Styles.success
    return (
        <>
            <div className="grid grid-cols-4 text-white">
                <div className="col-span-2 col-start-1 md:col-span-1 md:col-start-2">
                    <Link to={`${urlPrefix}/foods/view/${food.id}`}>
                        <Button text={food.title} buttonStyle={style} />
                    </Link>
                </div>
                <div
                    className="cols-start-2 col-span-2 md:col-span-1 md:col-start-3"
                    onClick={() => onDelete(food.id)}
                >
                    <Button
                        text={t('common.delete')}
                        buttonStyle={Styles.failure}
                    />
                </div>
            </div>
        </>
    )
}
const Foods = () => {
    const { t } = useTranslation(['home'])
    const [foods, setFoods] = useState<Food[]>([])

    useEffect(() => {
        const getFoods = async () => {
            const foodsFromApi = await fetchFoods()
            setFoods(foodsFromApi)
        }
        getFoods().then(() => console.log('foods fetched'))
    }, [])

    const fetchFoods = async () => {
        const response = await fetch(`${urlPrefix}/api/foods/get-all`)
        return await response.json()
    }

    const deleteFood = async (id: number) => {
        await fetch(`${urlPrefix}/api/foods/delete-food`, {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: JSON.stringify({ foodId: id }),
        })
        setFoods(foods.filter((food) => food.id !== id))
    }
    return (
        <div className="my-4">
            {foods.length > 0
                ? foods.map((food) => (
                      <FoodItem
                          food={food}
                          onDelete={deleteFood}
                          key={food.id}
                      />
                  ))
                : t('view.no_foods')}
        </div>
    )
}

export default Foods
