import '../../css/app.css'
import { Button } from './Button'
import { Link } from 'react-router-dom'
import {
    GiBellPepper,
    GiBowlOfRice,
    GiCarrot,
    GiCharcuterie,
    GiCheeseWedge,
    GiGrapes,
    GiHamburger,
    GiPizzaSlice,
    GiShinyApple,
    GiSlicedBread,
    GiSushis,
    GiTacos,
} from 'react-icons/gi'
import React, { useState } from 'react'
import { urlPrefix } from '../index'
import { Food } from './Foods'
import { IconType } from 'react-icons'
import { useTranslation } from 'react-i18next'

interface Icon {
    iconName: IconType
    iconColor: string
}

function FoodIcon() {
    let icons: Icon[] = [
        { iconName: GiPizzaSlice, iconColor: 'text-yellow-300' },
        { iconName: GiBowlOfRice, iconColor: 'text-yellow-50' },
        { iconName: GiCharcuterie, iconColor: 'text-yellow-200' },
        { iconName: GiCheeseWedge, iconColor: 'text-yellow-300' },
        { iconName: GiGrapes, iconColor: 'text-violet-500' },
        { iconName: GiShinyApple, iconColor: 'text-red-600' },
        { iconName: GiSlicedBread, iconColor: 'text-yellow-800' },
        { iconName: GiBellPepper, iconColor: 'text-red-600' },
        { iconName: GiCarrot, iconColor: 'text-orange-500' },
        { iconName: GiTacos, iconColor: 'text-yellow-200' },
        { iconName: GiHamburger, iconColor: 'text-amber-400' },
        { iconName: GiSushis, iconColor: 'text-red-300' },
    ]

    const randomIcon = icons[Math.floor(Math.random() * icons.length)]
    return (
        <randomIcon.iconName
            className={`h-32 w-32 ${randomIcon.iconColor} mx-auto`}
        ></randomIcon.iconName>
    )
}

const MainPage = () => {
    const { t } = useTranslation(['home'])

    const [decidedFood, setDecidedFood] = useState<Food>({
        cooldown: 0,
        cooldownPeriod: 0,
        cooldown_clears_at: null,
        error: false,
        id: 0,
        ingredients: [],
        note: '',
        title: '',
    })

    const getRandomFood = async () => {
        const decideResult = await fetch(`${urlPrefix}/api/foods/decide`)
        setDecidedFood(await decideResult.json())
    }

    const acceptFood = () => {
        fetch(`${urlPrefix}/api/foods/${decidedFood.id}/accept/`)
    }
    const DecidedFood = () => {
        return (
            <Link
                to={`${urlPrefix}/foods/view/${decidedFood.id}`}
                onClick={acceptFood}
            >
                <Button
                    text={t('index.decision_made', { food: decidedFood.title })}
                ></Button>
            </Link>
        )
    }

    return (
        <>
            <div className="grid grid-cols-5">
                <div className="col-span-3 col-start-2">
                    <FoodIcon></FoodIcon>
                    <h2 className="text-center text-white">
                        {t('index.welcome')}
                    </h2>
                    <Link to={`${urlPrefix}/foods/create`}>
                        <Button text={t('index.create_new_food')} />
                    </Link>
                    <Link to={`${urlPrefix}/foods/all`}>
                        <Button text={t('index.show_all_foods')} />
                    </Link>
                    <div onClick={getRandomFood}>
                        <Button text={t('index.decide')} />
                    </div>
                    {decidedFood.title ? <DecidedFood></DecidedFood> : <></>}
                </div>
            </div>
        </>
    )
}

export default MainPage
