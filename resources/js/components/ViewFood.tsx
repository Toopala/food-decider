import React, { ReactElement, useEffect, useState } from 'react'
import { useParams } from 'react-router'
import {
    MdAdd,
    MdAddShoppingCart,
    MdDeleteForever,
    MdDone,
    MdErrorOutline,
} from 'react-icons/md'
import { Button, SmallButton, Styles } from './Button'
import { Food, Ingredient } from './Foods'
import { urlPrefix } from '../index'
import { useTranslation } from 'react-i18next'

interface IngredientProps {
    ingredient?: Ingredient
    onSubmitStatus?: Function
    submitStatus?: ReactElement
}

function ViewFood() {
    const { t } = useTranslation(['home'])
    const { id } = useParams()
    const [buttonStatuses, setButtonStatuses] = useState([])
    const [food, setFood] = useState<Food>({
        cooldown: 0,
        cooldownPeriod: 0,
        cooldown_clears_at: null,
        isOnCooldown: false,
        error: false,
        id: 0,
        ingredients: [],
        note: '',
        title: '',
    })
    const [editMode, setEditMode] = useState(false)
    const [recipe, setRecipe] = useState(food.note)

    useEffect(() => {
        getFood()
    }, [])

    const getFood = async () => {
        const response = await fetch(`${urlPrefix}/api/foods/${id}/get/`)

        const food = await response.json().then((food): Food => {
            food.isOnCooldown = new Date() < new Date(food.cooldown_clears_at)
            return food
        })

        setFood(food)
        setRecipe(food.note)
    }

    const sendIngredientToShoppingList = async (
        ingredient: Ingredient,
        onSubmitStatus: Function,
        input: string
    ) => {
        let addResult = await fetch(`${urlPrefix}/api/shopping/add`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: JSON.stringify({
                id: ingredient.id,
                itemAmount: input,
            }),
        })

        if (addResult.status == 200) {
            onSubmitStatus(
                <SmallButton
                    icon={MdDone}
                    buttonStyle={Styles.successDisabled}
                />
            )
            return
        }
        if (addResult.status !== 200) {
            onSubmitStatus(
                <SmallButton
                    icon={MdErrorOutline}
                    buttonStyle={Styles.failureDisabled}
                />
            )
            return
        }
    }

    const removeIngredient = async (ingredient: Ingredient) => {
        let addResult = await fetch(
            `${urlPrefix}/api/foods/${food.id}/remove-ingredient/${ingredient.id}`,
            {
                method: 'DELETE',
            }
        )

        if (addResult.status == 200) {
            const newIngredients = food.ingredients.filter(
                (item) => item.id !== ingredient.id
            )
            setFood({ ...food, ingredients: newIngredients })
            return
        }
        if (addResult.status == 404) {
            return
        }
    }

    const IngredientItem: React.FC<IngredientProps> = ({
        ingredient,
        onSubmitStatus,
        submitStatus,
    }) => {
        const [input, setInput] = useState('')

        return (
            <div className="grid grid-cols-8 content-center gap-2 px-4">
                <div className="col-span-3 col-start-1 rounded-sm border-2 border-zinc-800 bg-zinc-800 px-3 py-2 text-zinc-500">
                    {ingredient.title}
                </div>
                <input
                    placeholder={t('view.amount')}
                    value={input}
                    onChange={(event) => setInput(event.target.value)}
                    className="col-span-3 h-10 rounded-sm border-2 border-zinc-800 bg-black bg-black px-3 text-white outline-none transition-all duration-300 ease-linear focus:rounded-3xl focus:border-lime-700"
                ></input>
                {editMode ? (
                    <SmallButton
                        icon={MdDeleteForever}
                        buttonStyle={Styles.failure}
                        onClick={() => removeIngredient(ingredient)}
                    />
                ) : (
                    <>
                        {submitStatus ?? (
                            <SmallButton
                                icon={MdAddShoppingCart}
                                onClick={() =>
                                    sendIngredientToShoppingList(
                                        ingredient,
                                        onSubmitStatus,
                                        input
                                    )
                                }
                            />
                        )}
                    </>
                )}
            </div>
        )
    }

    const handleResponse = (itemId: number, status: ReactElement) => {
        setButtonStatuses((prevStatuses) => ({
            ...prevStatuses,
            [itemId]: status,
        }))
    }

    const sendAllIngredientsToShoppingList = () => {
        food.ingredients.map((ingredient) =>
            sendIngredientToShoppingList(
                ingredient,
                (status: ReactElement) => handleResponse(ingredient.id, status),
                ''
            )
        )
    }

    const AddAllButton = () => {
        return (
            <div
                onClick={() => sendAllIngredientsToShoppingList()}
                className="col-span-1 col-start-1 mt-4"
            >
                <Button text={t('view.add_all_to_shopping_list')}></Button>
            </div>
        )
    }
    const saveRecipe = async (recipe: string) => {
        let saveResult = await fetch(
            `${urlPrefix}/api/foods/${food.id}/update-recipe`,
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json;charset=utf-8' },
                body: JSON.stringify({
                    recipe: recipe,
                }),
            }
        )

        if (saveResult.status == 200) {
            setFood({ ...food, note: recipe })
            setEditMode(false)
            return
        }
    }
    const Recipe = () => {
        const color = editMode ? 'white' : 'zinc-500'
        return (
            <>
                <div className="mt-4 grid grid-cols-5 px-4">
                    <h2 className={'text-white'}>{t('common.recipe')}:</h2>
                    {
                        <textarea
                            key={'recipe-input'}
                            value={recipe}
                            disabled={!editMode}
                            onChange={(event) => setRecipe(event.target.value)}
                            className={`px-3bg-black col-span-4 col-start-1 h-32 rounded-sm border-2 border-zinc-800 bg-black px-3 outline-none transition-all duration-300 ease-linear focus:rounded-3xl focus:border-lime-700 text-${color}`}
                        ></textarea>
                    }
                </div>
            </>
        )
    }

    const addIngredient = async (newIngredient: string) => {
        let saveResult = await fetch(
            `${urlPrefix}/api/foods/${food.id}/new-ingredient`,
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json;charset=utf-8' },
                body: JSON.stringify({
                    ingredientName: newIngredient,
                }),
            }
        )

        if (saveResult.status == 200) {
            const responseBody = await saveResult.json()
            const responseIngredient = {
                id: responseBody.id,
                title: responseBody.title,
            } as Ingredient

            const newIngredients = food.ingredients.concat(responseIngredient)
            setFood({ ...food, ingredients: newIngredients })
            return
        }
    }
    const AddNewIngredient = () => {
        const [newIngredient, setNewIngredient] = useState('')

        return (
            <>
                <h2 className="px-4 text-white">
                    {t('view.add_new_ingredient')}
                </h2>
                <div className="grid grid-cols-8 gap-2 px-4">
                    <input
                        placeholder={t('view.new_ingredient_placeholder')}
                        value={newIngredient}
                        onChange={(event) =>
                            setNewIngredient(event.target.value)
                        }
                        className="px-3bg-black col-span-4 h-10 rounded-sm border-2 border-zinc-800 bg-black px-3 text-white outline-none transition-all duration-300 ease-linear focus:rounded-3xl focus:border-lime-700"
                    ></input>
                    <div onClick={() => addIngredient(newIngredient)}>
                        <SmallButton icon={MdAdd} />
                    </div>
                </div>
            </>
        )
    }
    const clearCooldown = async () => {
        const saveResult = await fetch(
            `${urlPrefix}/api/foods/${food.id}/clear-cooldown`,
            {
                method: 'POST',
            }
        )

        if (saveResult.status == 200) {
            const updatedFood = (await saveResult.json()) as Food
            const isOnCooldown =
                new Date() < new Date(updatedFood.cooldown_clears_at)
            setFood({
                ...food,
                cooldown_clears_at: updatedFood.cooldown_clears_at,
                isOnCooldown: isOnCooldown,
            })
            return
        }
    }

    const CooldownInfo = () => {
        return (
            <>
                <div className="grid grid-cols-3">
                    <h2 className="mt-4 px-4 text-amber-600">{`karenssi päätty ${food.cooldown_clears_at}`}</h2>
                    <Button
                        text={t('view.clear_cooldown')}
                        buttonStyle={Styles.notice}
                        onClick={clearCooldown}
                    ></Button>
                </div>
            </>
        )
    }

    return (
        <>
            <h1
                className={`mt-4 px-4 text-2xl ${
                    food.isOnCooldown ? 'text-amber-600' : 'text-white'
                }`}
            >
                {' '}
                {food.title}{' '}
            </h1>
            {food.isOnCooldown && <CooldownInfo></CooldownInfo>}
            <div className="mt-7">
                {food.ingredients
                    ? food.ingredients.map((ingredient) => (
                          <IngredientItem
                              key={ingredient.id}
                              ingredient={ingredient}
                              onSubmitStatus={(status: ReactElement) =>
                                  handleResponse(ingredient.id, status)
                              }
                              submitStatus={buttonStatuses[ingredient.id]}
                          />
                      ))
                    : 'No ingredients'}
            </div>
            <div>
                {!editMode && <AddAllButton></AddAllButton>}
                <Recipe></Recipe>
            </div>
            <div
                onClick={() => setEditMode(!editMode)}
                className="col-span-1 col-start-1"
            >
                {editMode ? (
                    <>
                        <div className="my-4">
                            <AddNewIngredient></AddNewIngredient>
                        </div>
                        <div className="my-4"></div>

                        <div onClick={() => setRecipe(food.note)}>
                            <Button
                                text={t('common.cancel')}
                                buttonStyle={Styles.edit}
                            ></Button>
                        </div>
                        <div onClick={() => saveRecipe(recipe)}>
                            <Button
                                text={t('common.save')}
                                buttonStyle={Styles.success}
                            ></Button>
                        </div>
                    </>
                ) : (
                    <Button
                        text={t('common.edit')}
                        buttonStyle={Styles.edit}
                    ></Button>
                )}
            </div>
        </>
    )
}

export default ViewFood
