import React from 'react'
import { IconType } from 'react-icons'

interface ButtonProps {
    text: string
    buttonStyle?: Styles
    onClick?: () => void
}

interface SmallButtonProps {
    icon: IconType
    buttonStyle?: Styles
    onClick?: () => void
}

export enum Styles {
    success,
    successDisabled,
    failure,
    failureDisabled,
    edit,
    notice,
}

const getButtonFlavour = (buttonStyle: Styles) => {
    const baseButton =
        'bg-transparent border-solid border-2 bg-black rounded-sm text-white hover:rounded-3xl transition-all ease-linear duration-300 h-9 text-center align-middle min-w-full'
    const baseDisabledButton =
        'bg-transparent border-solid border-2 bg-black rounded-sm text-white rounded-3xl  h-9 text-center align-middle min-w-full'

    switch (buttonStyle) {
        case Styles.failure:
            return `${baseButton} border-red-600 hover:bg-red-600 hover:border-red-600`
        case Styles.failureDisabled:
            return `${baseDisabledButton} border-red-600 bg-red-600 disabled`
        case Styles.edit:
            return `${baseButton} border-sky-600 hover:bg-sky-600 hover:border-sky-600`
        case Styles.notice:
            return `${baseButton} border-amber-600 hover:bg-amber-600 hover:border-amber-600`
        case Styles.successDisabled:
            return `${baseDisabledButton} border-lime-600 bg-lime-600 disabled`
        default:
            return `${baseButton} border-lime-600 hover:bg-lime-600 hover:border-lime-600`
    }
}

const isDisabled = (style: Styles): boolean => {
    return [Styles.failureDisabled, Styles.successDisabled].includes(style)
}

export const Button: React.FC<ButtonProps> = (props) => {
    return (
        <div className="px-4 py-1">
            <button
                className={getButtonFlavour(props.buttonStyle)}
                disabled={isDisabled(props.buttonStyle)}
                onClick={props.onClick}
            >
                {props.text}
            </button>
        </div>
    )
}
export const SmallButton: React.FC<SmallButtonProps> = (props) => {
    return (
        <div className={'h-10 w-10 justify-items-stretch'}>
            <button
                disabled={isDisabled(props.buttonStyle)}
                className={`${getButtonFlavour(props.buttonStyle)}`}
                onClick={props.onClick}
            >
                <div className={'pl-1.5'}>
                    <props.icon size={25} />
                </div>
            </button>
        </div>
    )
}
