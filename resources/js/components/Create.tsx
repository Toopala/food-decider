import React, { useEffect, useState } from 'react'
import { Button, SmallButton, Styles } from './Button'
import { Link } from 'react-router-dom'
import { Food, Ingredient } from './Foods'
import { urlPrefix } from '../index'
import { useTranslation } from 'react-i18next'
import { MdAdd } from 'react-icons/md'

interface Option {
    value: number
    label: string
}

interface TextInputs {
    title: string
    cooldown: number
    cooldownPeriod: number
    notes: string
}

function Create() {
    const { t } = useTranslation(['home'])
    const selectOptions: Option[] = [
        { value: 0, label: t('create.days') },
        { value: 1, label: t('create.weeks') },
        { value: 2, label: t('create.months') },
        { value: 3, label: t('create.years') },
    ]
    const [selectedOption, setSelectedOption] = useState<Option>({
        label: '',
        value: 0,
    })
    const [textInputs, setTextInputs] = useState<TextInputs>({
        title: '',
        cooldown: 1,
        cooldownPeriod: 1,
        notes: '',
    })
    const [ingredientInput, setIngredientInput] = useState<string>('')
    const [ingredientTags, setIngredientTags] = useState<string[]>([])
    const [newFood, setNewFood] = useState<Food>({
        cooldown: 0,
        cooldownPeriod: 0,
        cooldown_clears_at: null,
        error: false,
        id: 0,
        ingredients: [],
        note: '',
        title: '',
    })
    const [textareaInput, setTextareaInput] = useState('')
    const [ingredients, setIngredients] = useState<Ingredient[]>([])
    useEffect(() => {
        setSelectedOption(selectOptions[0])
        const getIngredients = async () => {
            const ingredientsFromApi = await fetchIngredients()
            setIngredients(ingredientsFromApi)
        }
        getIngredients().then(() => {})
    }, [])

    const fetchIngredients = async () => {
        const response = await fetch(`${urlPrefix}/api/ingredients/get-all`)
        return await response.json()
    }

    const selectChange = (event: React.ChangeEvent<any>) => {
        const value = event.target.value
        setSelectedOption(value)
    }

    const handleTextChange = (event: React.ChangeEvent<any>) => {
        const name = event.target.name
        const value = event.target.value
        setTextInputs((values: TextInputs) => ({ ...values, [name]: value }))
    }

    const handleTextareaChange = (event: React.ChangeEvent<any>) => {
        setTextareaInput(event.target.value)
    }
    const submitForm = async (event: React.ChangeEvent<any>) => {
        event.preventDefault()
        let createdFood = await fetch(`${urlPrefix}/api/foods/create`, {
            method: `POST`,
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: JSON.stringify({
                title: textInputs.title,
                ingredients: ingredientTags.join(','),
                cooldown: textInputs.cooldown || 1,
                cooldownPeriod: selectedOption.value,
                notes: textareaInput,
            }),
        })

        if (createdFood.status !== 200) {
            let fail = await createdFood.json()
            fail.error = true
            setNewFood(fail)
            return
        }

        let success = await createdFood.json()
        success.error = false
        setNewFood(success)
    }

    function handleSuggestionClick(suggestion: Ingredient) {
        return () => {
            setIngredientTags([...ingredientTags, suggestion.title])
            setIngredients(
                ingredients.filter((item) => item.title !== suggestion.title)
            )
        }
    }

    const renderSuggestions = () => {
        let filteredItems: Ingredient[]
        filteredItems =
            ingredientInput.length > 2
                ? ingredients.filter(
                      (item) =>
                          item.title
                              .toLowerCase()
                              .indexOf(ingredientInput.toLowerCase()) !== -1
                  )
                : []
        return (
            <div className="flex">
                <ul className="flex-col">
                    {filteredItems.map((item, index) => (
                        <li
                            className="my-2 flex-auto rounded-sm border-2 border-solid border-lime-600 bg-black bg-transparent px-2 text-white transition-all duration-300 ease-linear hover:rounded-3xl hover:border-lime-600 hover:bg-lime-600"
                            key={`suggestion-${index}`}
                            onClick={handleSuggestionClick(item)}
                        >
                            {item.title}
                        </li>
                    ))}
                </ul>
            </div>
        )
    }

    const handleIngredientInputChange = (event: React.ChangeEvent<any>) => {
        setIngredientInput(event.target.value.trim())
    }

    function addSuggestedIngredient() {
        const newIngredient = {
            id: Math.max(...ingredients.map((item) => item.id)) + 1,
            title: ingredientInput,
        }
        setIngredientTags([...ingredientTags, newIngredient.title])
        setIngredients(
            ingredients.filter((item) => item.title !== newIngredient.title)
        )
        setIngredientInput('')
    }

    const handleIngredientKeyDown = (
        event: React.KeyboardEvent<HTMLInputElement>
    ) => {
        //@ts-ignore
        const input = event.target.defaultValue.trim()
        if ((event.key === ' ' || event.code === 'Space') && input.length > 0) {
            addSuggestedIngredient()
        }
    }

    const handleTagDelete = (index: number, tag: string) => {
        setIngredientTags([
            ...ingredientTags.slice(0, index),
            ...ingredientTags.slice(index + 1),
        ])
        const filteredTags = ingredients.filter((item) => item.title === tag)
        const deletedTag =
            filteredTags.length === 0
                ? { id: 999, title: tag }
                : filteredTags[0]
        setIngredients((prevArray) => [...prevArray, deletedTag])
    }

    const renderTag = (tag: any, index: number) => {
        return (
            <span
                key={`tag-${index}`}
                className="mx-1 my-2 justify-center rounded-sm border-2 border-solid border-lime-600 bg-black bg-transparent px-2 text-center text-white transition-all duration-300 ease-linear hover:rounded-3xl hover:border-red-600 hover:bg-red-600"
                onClick={() => handleTagDelete(index, tag)}
            >
                {tag}
            </span>
        )
    }

    return (
        <>
            <form
                className="mt-4 grid grid-cols-1 px-10 text-white"
                onSubmit={submitForm}
            >
                <label className="col-span-1 mt-3">
                    {t('create.food_title')}
                </label>
                <input
                    type="text"
                    className="col-span-1 h-10 rounded-sm border-2 border-zinc-800 bg-black px-3 outline-none transition-all duration-300 ease-linear focus:rounded-3xl focus:border-lime-700"
                    name="title"
                    value={textInputs.title || ''}
                    onChange={handleTextChange}
                    placeholder={t('create.food_title_placeholder')}
                />
                <label className="col-span-1 mt-3">
                    {t('common.ingredients')}
                </label>
                <div className="flex flex-wrap">
                    {ingredientTags.map(renderTag)}
                </div>
                <span className="my-3 flex">
                    <input
                        type="text"
                        className="h-10 flex-auto rounded-sm border-2 border-zinc-800 bg-black px-3 outline-none transition-all duration-300 ease-linear focus:rounded-3xl focus:border-lime-700 "
                        value={ingredientInput || ''}
                        onChange={handleIngredientInputChange}
                        placeholder={t('create.ingredients_placeholder')}
                        onKeyUp={handleIngredientKeyDown}
                    />
                    <SmallButton
                        icon={MdAdd}
                        onClick={addSuggestedIngredient}
                    />
                </span>
                {ingredientInput !== '' && renderSuggestions()}
                <label className="col-span-1 mt-3">
                    {t('create.cooldown')}
                </label>
                <input
                    type="text"
                    className="col-span-1 h-10 rounded-sm border-2 border-zinc-800 bg-black px-3 outline-none transition-all duration-300 ease-linear focus:rounded-3xl focus:border-lime-700"
                    name="cooldown"
                    value={textInputs.cooldown || 1}
                    onChange={handleTextChange}
                />
                <label className="col-span-1 mt-3">
                    {t('create.unit_of_time')}
                </label>
                <select
                    className="col-span-1 h-10 rounded-sm border-2 border-zinc-800 bg-black px-3 outline-none transition-all duration-300 ease-linear focus:rounded-3xl focus:border-lime-700"
                    name="cooldownPeriod"
                    value={selectedOption.value}
                    onChange={selectChange}
                >
                    {selectOptions.map((option, index) => (
                        <option key={index} value={option.value}>
                            {' '}
                            {option.label}{' '}
                        </option>
                    ))}
                </select>
                <label className="col-span-1 mt-3">{t('common.recipe')}</label>
                <textarea
                    className="col-span-1 h-10 rounded-sm border-2 border-zinc-800 bg-black px-3 outline-none transition-all duration-300 ease-linear focus:rounded-3xl focus:border-lime-700"
                    style={{ height: 150 + 'px' }}
                    name="recipe"
                    value={textareaInput}
                    onChange={handleTextareaChange}
                />
                <div className="col-span-1">
                    <Button text={t('common.submit')}></Button>
                </div>
                {newFood.title && (
                    <div className="col-span-1">
                        <Link to={`${urlPrefix}/foods/view/` + newFood.id}>
                            <Button
                                text={
                                    newFood.error
                                        ? t('create.food_create_failed', {
                                              food: newFood.title,
                                          })
                                        : t('create.food_created', {
                                              food: newFood.title,
                                          })
                                }
                                buttonStyle={
                                    newFood.error
                                        ? Styles.failure
                                        : Styles.success
                                }
                            ></Button>
                        </Link>
                    </div>
                )}
            </form>
        </>
    )
}

export default Create
