# Food decider
You know that you have serious first world problems, when one of the most irritating questions in our household is:
"What should we eat next?", and the tedium which follows when we try to come to an agreement.

Thus, Food decider was born. It allows to you to save your favourite meals, and randomly decide which food to prepare next

## Features
- Store favourite foods
- Set required ingredients for the meal
- Upload ingredients to [Homeassistant shopping list](https://www.home-assistant.io/integrations/shopping_list/)
  - Install as Homeassistant addon: https://gitlab.com/Toopala/food-decider-homeassistant#

## Planned features
- Set cooldown to foods, so you can determine how frequently it can be suggested
- Tags to ingredients and foods
  - Maybe you want to decide food with specific type of ingredient
    - Fish or maybe vegan?
    - Or maybe more time-consuming meals to prep during weekend?
- Maybe integration to other checklists:
  - Microsoft ToDo
  - Google keep
  - etc
- Login and user management to host outside a Homeassistant instance
